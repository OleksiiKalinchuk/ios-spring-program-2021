import UIKit

//MARK: - Task 4.2
//Реалізувати структуру для представлення комплексних чисел
struct ComplexNumber {
    var real: Double
    var image: Double
}

//MARK: - Task 4.3
//Реалізувати оператори для роботи з комплексними числами
indirect enum ComplexOperators {
    case number(ComplexNumber)
    case addition(ComplexOperators, ComplexOperators)
    case subtraction(ComplexOperators, ComplexOperators)
    case multiply(ComplexOperators, ComplexOperators)
    case divide(ComplexOperators, ComplexOperators)
}

func + (left: ComplexNumber, right: ComplexNumber) -> ComplexNumber {
    let realNum = left.real + right.real
    let imageNum = left.image + right.image
    return ComplexNumber(real: realNum, image: imageNum)
}

func - (left: ComplexNumber, right: ComplexNumber) -> ComplexNumber {
    let realNum = left.real - right.real
    let imageNum = left.image - right.image
    return ComplexNumber(real: realNum, image: imageNum)
}

func * (left: ComplexNumber, right: ComplexNumber) -> ComplexNumber {
    let realNum = (left.real * right.real) - (left.image * right.image)
    let imageNum = (left.real * right.image) + (left.image * right.real)
    return ComplexNumber(real: realNum, image: imageNum)
}

func / (left: ComplexNumber, right: ComplexNumber) -> ComplexNumber {
    
    let commonDivider = pow(right.real, 2) + pow(right.image, 2)
    
    let realNum = (left.real * right.real + left.image * right.image) / commonDivider
    let imageNum = -1 * ((left.real * right.image - left.image * right.real) / commonDivider)
    return ComplexNumber(real: realNum, image: imageNum)
}


func evaluate(_ expression: ComplexOperators) -> ComplexNumber {
    switch expression {
    case let .number(complexNumber):
        return complexNumber
    case let .addition(left, right):
        return evaluate(left) + evaluate(right)
    case let .subtraction(left, right):
        return evaluate(left) - evaluate(right)
    case let .multiply(left, right):
        return evaluate(left) * evaluate(right)
    case let .divide(left, right):
        return evaluate(left) / evaluate(right)
    }
}

let cNum1 = ComplexNumber(real: 5, image: 3)
let cNum2 = ComplexNumber(real: 2, image: 1)

let expressionSubtraction = ComplexOperators.subtraction(ComplexOperators.number(cNum1), ComplexOperators.number(cNum2))

let expressionAddition = ComplexOperators.addition(ComplexOperators.number(cNum1), ComplexOperators.number(cNum2))

let expressionMultiply = ComplexOperators.multiply(ComplexOperators.number(cNum1), ComplexOperators.number(cNum2))

let expressionDivide = ComplexOperators.divide(ComplexOperators.number(cNum1), ComplexOperators.number(cNum2))

let resSubtraction = evaluate(expressionSubtraction)
let resAddition = evaluate(expressionAddition)
let resMultiply = evaluate(expressionMultiply)
let resDivide = evaluate(expressionDivide)

print("Result of subtraction of 2 complex numbers: realPart = \(resSubtraction.real), imagePart = \(resSubtraction.image)")

print("Result of addition of 2 complex numbers: realPart = \(resAddition.real), imagePart = \(resAddition.image)")

print("Result of multiply of 2 complex numbers: realPart = \(resMultiply.real), imagePart = \(resMultiply.image)")

print("Result of division of 2 complex numbers: realPart = \(resDivide.real), imagePart = \(resDivide.image)")

//MARK: - Task 4.4
//Функція що розвязує квадратне рівняння(в якості аргументів Double) і повертає розвязки tuple де значення будуть мати тип з попереднього завдання.


func quadraticEquation(a: Double, b: Double, c: Double) -> (root1: ComplexNumber, root2: ComplexNumber)? {
    
    var discriminant = (b * b) - (4 * a * c)
    
    if discriminant < 0 {
        
        discriminant *= -1
        
        let rootReal = -b / (2 * a)
        let root1Image = -sqrt(discriminant) / (2 * a)
        let root2Image = sqrt(discriminant) / (2 * a)
        
        let result = (root1: ComplexNumber(real: rootReal, image: root1Image), root2: ComplexNumber(real: rootReal, image: root2Image))
        
        return result
    }
    return nil
}

let resultOfEquation = quadraticEquation(a: 1, b: -6, c: 34)

if let res = resultOfEquation {
    print("Result of equation - root1: real = \(res.root1.real), image = \(res.root1.image), root2: real = \(res.root2.real), image = \(res.root2.image)")
} else {
    print("discriminant >= 0")
}
