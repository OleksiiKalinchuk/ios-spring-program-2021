import UIKit

//MARK: - Task with init struct class
struct Employee {
    var firstName: String
    var lastName: String
}

struct Product {
    var name: String
}

class Company {
    var list: [Employee]
    var name: String

    required init() {
        list = []
        name = ""
    }


    init(list: [Employee], name: String) {
        self.list = list
        self.name = name
    }

    convenience init?(employee: Employee?, name: String) {
        if let safeEmployee = employee {
            self.init(list: [safeEmployee], name: name)
        } else {
            return nil
        }
    }

    func getProduct() -> Product {
        return Product(name: name)
    }

}

class StateRegistry {

    static var reqisteredCompanies = [Company]()

    func addCompany(company: Company?) {
        if let safeCompany = company {
            StateRegistry.reqisteredCompanies.append(safeCompany)
        }
    }

    func isCompanyRegistered(name: String) -> Bool {

        if StateRegistry.reqisteredCompanies.contains(where: { company in
            company.name == name
        }) {
            return true
        }
        return false
    }
}

class FoodCompany: Company {

    var qualityCertificate: String

    init?(employee: (firstName: String?, secondName: String?), name: String, qualityCertificate: String) {

        if let safeFirstName = employee.firstName, let safeSecondName = employee.secondName {

            if !name.isEmpty, !qualityCertificate.isEmpty, !safeFirstName.isEmpty, !safeSecondName.isEmpty {

                self.qualityCertificate = qualityCertificate

                super.init(list: [Employee(firstName: safeFirstName, lastName: safeSecondName)], name: name)
            } else {
                return nil
            }
        } else {
            return nil
        }
    }

    required init() {
        self.qualityCertificate = ""
        super.init()
    }

}

class Project {
    var contractor: Company
    var name: String

    init?(name: String, company: Company) {

        if StateRegistry().isCompanyRegistered(name: company.name) {
            self.name = name
            contractor = company
        } else {
            return nil
        }
    }
}

var employeeOfSecondCompany = Employee(firstName: "Stas", lastName: "Rokitin")
var employeeOfThirdCompany1 = Employee(firstName: "Oleh", lastName: "Tkach")
var employeeOfThirdCompany2 = Employee(firstName: "Stepan", lastName: "Kitch")
var employeeOfFoodCompany2 = (firstName: "Oleksii", secondName: "Kindrok")

var firstCompany = Company()
var secondCompany = Company(employee: employeeOfSecondCompany, name: "Second company")
var thirdCompany = Company(list: [employeeOfThirdCompany1, employeeOfThirdCompany2], name: "Third company")
var foodCompany1 = FoodCompany()
var foodCompany2 = FoodCompany(employee: employeeOfFoodCompany2, name: "KFC", qualityCertificate: "C2")

var register = StateRegistry()
register.addCompany(company: firstCompany)
register.addCompany(company: secondCompany)
register.addCompany(company: thirdCompany)
register.addCompany(company: foodCompany1)
register.addCompany(company: foodCompany2)

if let project1 = Project(name: "Project of First company", company: firstCompany) {
    print("Name of company 1: \(project1.contractor.name)")
    print("List of employee: \(project1.contractor.list)")
    print("Project name: \(project1.name)")
}

if let safeSecondCompany = secondCompany {
    if let project2 = Project(name: "Project of second company", company: safeSecondCompany) {
        print("Name of company 2: \(project2.contractor.name)")
        print("List of employee: \(project2.contractor.list)")
        print("Project name: \(project2.name)")
    }
}
if let project3 = Project(name: "Project of Third company", company: thirdCompany) {
    print("Name of company 3: \(project3.contractor.name)")
    print("List of employee: \(project3.contractor.list)")
    print("Project name: \(project3.name)")
}

if let projectFood1 = Project(name: "Project of 1 food company", company: foodCompany1) {
    print("Name of food company 1: \(projectFood1.contractor.name)")
    print("List of employee: \(projectFood1.contractor.list)")
    print("Project name: \(projectFood1.name)")
}

if let safeFoodCompany2 = foodCompany2 {
    if let projectFood2 = Project(name: "Project of 2 food company", company: safeFoodCompany2) {
        print("Name of food company 2: \(projectFood2.contractor.name)")
        print("List of employee: \(projectFood2.contractor.list)")
        print("Project name: \(projectFood2.name)")
    }
}

var emptyEmployee: Employee? = nil
let nilCompany = Company(employee: emptyEmployee, name: "Monobank")
register.addCompany(company: nilCompany)
print(register.isCompanyRegistered(name: "Monobank"))
