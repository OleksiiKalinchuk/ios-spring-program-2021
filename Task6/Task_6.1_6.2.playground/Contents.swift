import UIKit

//MARK: - Task 6.1
//Реалізувати свій Optional(MyOptional).
enum MyOptional<T> {

    case value(T)
    case none

    func upwrap() -> T? {
        switch self {
        case .value(let val):
            return val
        default:
            return nil
        }
    }
}

var myOptional = MyOptional.value("test")
print("Optional with value = \(myOptional.upwrap())")

myOptional = .none
print("Optional without value = \(myOptional.upwrap())")

//MARK: - Task 6.2
//Реалізувати функцію,яка в якості аргументів буде приймати 2 числа типу MyOptional<Int> і повертати суму цих чисел також типу MyOptional<Int>.

extension MyOptional where T: Numeric {

    static func + (left: MyOptional, right: MyOptional) -> MyOptional {
        if let safeLeft = left.upwrap(), let safeRight = right.upwrap() {
            return MyOptional.value(safeLeft + safeRight)
        }
        return MyOptional.none
    }
}

var n1 = MyOptional.value(4)
var n2 = MyOptional.value(3)

let res = n1 + n2
print("Result of sum of \(n1.upwrap()) and \(n2.upwrap()) = \(res.upwrap())")

