import UIKit

var a = 2, b = 2, c = 2, d = 2

func arithmeticMean(a: Int, b: Int, c: Int, d: Int) -> Int {
    return (a + b + c + d) / 4
}

func geometricMean(a: Int, b: Int, c: Int, d: Int) -> Double {
    return pow(Double(a * b * c * d), 1/4)
}

let resultArithmetic = arithmeticMean(a: a, b: b, c: c, d: d)
print("Arithmetic mean: \(resultArithmetic)")

let resultGeometric = geometricMean(a: a, b: b, c: c, d: d)
print("Geometric mean: \(resultGeometric)")
