import UIKit

indirect enum LinkedList<T> {
    case Node(element: T, next: LinkedList<T>)
    case Empty

    init() {
        self = .Empty
    }
    
    mutating func append(value: T) {
        self = .Node(element: value, next: self)
    }
    
    func description () -> String {
        var result = String()
        var current: LinkedList<T>?  = self
        while current != nil {
            switch current {
            case let .Node(value, next):
                current = next
                result += "\(value)"
            default:
                current = nil
            }
        }
        return String(result.reversed())
    }
}

var list = LinkedList<Int>()
list.append(value: 1)
list.append(value: 2)
list.append(value: 3)
list.append(value: 4)
print(list.description())
