import UIKit

let allStudents: Set = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let presentOnMonday: Set = [1, 2, 5, 6, 7]
let presentOnTuesday: Set = [3, 6, 8, 10]
let presentOnWednesday: Set = [1, 3, 7, 9, 10]

func studentsWhoAttendThreeDays() -> Set<Int> {
    
    let attendMondayAndTuesday: Set = presentOnMonday.intersection(presentOnTuesday)
    let attendThreeDays: Set = attendMondayAndTuesday.intersection(presentOnWednesday)
    
    return attendThreeDays
}

func studentsWhoAttendTwoDays() -> Set<Int> {
    
    let attendMondayAndTuesday = presentOnMonday.intersection(presentOnTuesday)
    let attendMondayAndWednesday = presentOnMonday.intersection(presentOnWednesday)
    let attendTuesdayAndWednesday = presentOnTuesday.intersection(presentOnWednesday)
    
    let twoDays = attendMondayAndTuesday.union(attendMondayAndWednesday.union(attendTuesdayAndWednesday))
    let result = studentsWhoAttendThreeDays().symmetricDifference(twoDays)
    return result
}

func studentsWhoAttendMondayAndWednesdayButNotTuesday() -> Set<Int> {
    
    let attendMondayAndWednesday = presentOnMonday.intersection(presentOnWednesday)
    let missedTuesday = allStudents.symmetricDifference(presentOnTuesday)
    let result = attendMondayAndWednesday.intersection(missedTuesday)
    
    return result
}

func studentsWhoMissedAllClasses() -> Set<Int> {
    
    let mondayOrTuesdayPresent = presentOnMonday.union(presentOnTuesday)
    let studentsThatAttendAtLeastOne = mondayOrTuesdayPresent.union(presentOnWednesday)
    let missedAllDays = allStudents.symmetricDifference(studentsThatAttendAtLeastOne)
    
    return missedAllDays
}

print("Attend 3 days: \(studentsWhoAttendThreeDays())")
print("Attend 2 days: \(studentsWhoAttendTwoDays())")
print("Missed all classes \(studentsWhoMissedAllClasses())")
print("Attenf Monday and Wednesday but not Tuesday \(studentsWhoAttendMondayAndWednesdayButNotTuesday())")
