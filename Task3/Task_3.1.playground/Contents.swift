import UIKit

let allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for student in allStudents {
    print(student)
}

var i = 0
while i < allStudents.count {
    print(allStudents[i])
    i += 1
}

i = 0
repeat {
    print(allStudents[i])
    i += 1
} while i < allStudents.count
