import UIKit

let allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for students in allStudents {
    if students % 2 == 0 {
        print(students)
    }
}

for students in allStudents {
    if students % 2 == 1 {
        print(students)
    }
}

var i = 0
while i < allStudents.count{
    if allStudents[i] % 2 == 0 {
        print(allStudents[i])
    }
    i += 1
}

i = 0
while i < allStudents.count {
    if allStudents[i] % 2 == 1 {
        print(allStudents[i])
    }
    i += 1
}

i = 0
repeat {
    if allStudents[i] % 2 == 0 {
        print(allStudents[i])
    }
    i += 1
} while i < allStudents.count

i = 0
repeat {
    if allStudents[i] % 2 == 1 {
        print(allStudents[i])
    }
    i += 1
} while i < allStudents.count


