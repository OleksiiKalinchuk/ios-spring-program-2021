import UIKit

let array = [1, 2, 12, 5, 7, 88]

//MARK: - Task 5.1
let evenNumbers = array.filter { $0 % 2 == 0 }
print("Task 5.1: only even numbers: \(evenNumbers)")



//MARK: - Task 5.2
let sum = array.reduce(0) { sum, num in
    sum + num
}
print("Task 5.2: sum of numbers in array: \(sum)")



//MARK: - Task 5.3
let arrStrings = ["1", "2", "12", "bla", "5", "7", "88"]

let arrInt = arrStrings.compactMap { Int($0) }
print("Task 5.3: Converted to Int: \(arrInt)")
