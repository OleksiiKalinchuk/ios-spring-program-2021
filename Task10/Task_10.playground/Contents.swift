import UIKit

//MARK: - Part 1

//Реалізувати enum Transmission (варіатор, типтронік, робот, ручна)
enum Transmissiom {
    case variator
    case tiptronik
    case robot
    case manual
}

//Реалізувати структуру -Car з властивістями модель (String), потужність (Int), коробка передач (Transmission).
struct Car {
    var model: String
    var power: Int
    var transmission: Transmissiom
}

//Додати пропертю description за допомогою розширення структури Car протоколом CustomStringConvertible.
extension Car: CustomStringConvertible {
    var description: String {
        return "Model: \(model), power: \(power), transmissiom: \(transmission)"
    }
}

//Реалізувати фукцію яка буде приймати массив з машинами і повертати масив масивів з автомобілями ([[Car]]) групованих по типу коробки передач
func sortedByTransmission(cars: [Car]) -> [[Car]] {

    var sortedCars = [[Car]]()
    var carWithVariator = [Car](), carWithTiptronik = [Car](), carWithRobot = [Car](), carWithManual = [Car]()

    for car in cars {
        switch car.transmission {
        case .variator:
            carWithVariator.append(car)
        case .tiptronik:
            carWithTiptronik.append(car)
        case .robot:
            carWithRobot.append(car)
        default:
            carWithManual.append(car)
        }
    }

    sortedCars.append(carWithVariator)
    sortedCars.append(carWithTiptronik)
    sortedCars.append(carWithRobot)
    sortedCars.append(carWithManual)

    return sortedCars
}

//Створіть массив з машинами в яких різні коробки передач.
var car1 = Car(model: "Nissan", power: 2, transmission: .variator)
var car2 = Car(model: "Mazda", power: 3, transmission: .manual)
var car3 = Car(model: "BMW", power: 1, transmission: .robot)
var car4 = Car(model: "Mersedes", power: 4, transmission: .tiptronik)
var car5 = Car(model: "Niva", power: 5, transmission: .variator)

let carsArray = [car1, car2, car3, car4, car5]

//Передайте масив в створену Вами функцію і виведіть результат з використанням змінної description
let result = sortedByTransmission(cars: carsArray)

print("Result of 1 part: \(result)")

//MARK: - Part 2

//Додати екстеншен для Transmissionв якому іплементувати протокол Hashable

extension Transmissiom: Hashable {

    func hash(into hasher: inout Hasher) {

        switch self {
        case .manual:
            hasher.combine(0)
        case .robot:
            hasher.combine(1)
        case .tiptronik:
            hasher.combine(2)
        case .variator:
            hasher.combine(3)
        }
    }
    
}

//Реалізувати в екстеншені массиву функцію group(by: (element: Element) -> AnyHashable) -> [[Element]]
extension Array {
    
    func group(by: (_ element: Element) -> AnyHashable) -> [[Element]] {
        
        var sortedCars = [[Element]]()
        var carWithVariator = [Element](), carWithTiptronik = [Element](), carWithRobot = [Element](), carWithManual = [Element]()

        for item in self {
            switch by(item).hashValue {
            case Transmissiom.manual.hashValue:
                carWithManual.append(item)
            case Transmissiom.robot.hashValue:
                carWithRobot.append(item)
            case Transmissiom.tiptronik.hashValue:
                carWithTiptronik.append(item)
            default:
                carWithVariator.append(item)
            }
        }

        sortedCars.append(carWithVariator)
        sortedCars.append(carWithTiptronik)
        sortedCars.append(carWithRobot)
        sortedCars.append(carWithManual)
        
        return sortedCars
    }
}

//Визвіть на цьому массиві свою функцію і в неї в якості аргументу передайте кложуру яка при отриманні елементу (Машини) буде повертати хеш для коробки передач
let result2 = carsArray.group { car in
    return car.transmission
}

//Виведіть результат з використанням змінної description
print("Result of 2 part: \(result2)")
