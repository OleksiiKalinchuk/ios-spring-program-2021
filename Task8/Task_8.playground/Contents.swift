import UIKit

class Tree {
    
    private var top: Node?
    
    func add(_ element: Int) {
        
        guard top != nil else {
            top = Node(element)
            return
        }
        
        addRecurs(node: &top, element: element)
    }
    
    private func addRecurs(node: inout Node?, element: Int) {
        if let n = node {
            if n.key < element {
                addRecurs(node: &n.left, element: element)
                if n.left == nil {
                    n.left = Node(element)
                }
            } else if n.key > element {
                addRecurs(node: &n.right, element: element)
                if n.right == nil {
                    n.right = Node(element)
                }
            }
        }
    }
    
    func printAllElements() {
        if let t = top {
            printElements(node: t)
        }
    }
    
    private func printElements(node: Node?) {
        if let n = node {
            if n.right != nil {
                printElements(node: n.right)
            }
            print(n.key)
            if n.left != nil {
                printElements(node: n.left)
            }
            
        }
    }
    
    private class Node {
        
        var key: Int
        var left: Node?
        var right: Node?
        
        init(_ e: Int) {
            key = e
        }
    }
}



var tree = Tree()
tree.add(3)
tree.add(2)
tree.add(4)
tree.add(1)
tree.add(5)
tree.printAllElements()
